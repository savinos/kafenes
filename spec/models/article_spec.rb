require 'rails_helper'

RSpec.describe Article, type: :model do

  # TODO replace url for testing to localhosts
  context "Δεδομένου URL άρθρου που ΔΕΝ είναι αναγνώσιμο από την εφαρμογή" do
    it "το @article δεν είναι έγκυρο" do
      parameters = {}
      parameters[:url] = "http://www.wired.com/2016/05/adblock-plus-now-wants-pay-browse-internet/"      
      @article = Article.new(parameters)
      expect(@article.valid?).to eq false
    end
  end

  context "Δεδομένου URL άρθρου που είναι αναγνώσιμο από την εφαρμογή" do
    it "το @article είναι έγκυρο" do
      parameters = {}
      parameters[:url] = "http://www.theguardian.com/australia-news/2016/may/03/asylum-seekers-set-themselves-alight-nauru"      
      @article = Article.new(parameters)
      expect(@article.valid?).to eq true
    end
  end
  
  context "Δεδομένου URL άρθρου με ορίσματα στο τέλος" do
    it "αφαιρούνται" do
      parameters = {}
      parameters[:url] = "http://www.dailymail.co.uk/news/article-3569986/American-artist-transforms-family-four-characters-Lion-King-digitally-enhanced-photographs.html?ITO=1490&ns_mchannel=rss&ns_campaign=1490"      
      @article = Article.new(parameters)
      @article.save
      expect(Article.last.url).to eq "http://www.dailymail.co.uk/news/article-3569986/American-artist-transforms-family-four-characters-Lion-King-digitally-enhanced-photographs.html"
    end
  end


    context "Δεδομένου έγκυρου άρθρου" do
    it "αποθηκευόνται αυτόματα, ο τίτλος, το περιεχόμενο και η γλώσσα" do
      parameters = {}
      parameters[:url] = "http://www.theguardian.com/world/2016/may/03/european-commission-turkish-citizens-visa-free-travel-schengen"
      parameters[:published_at] = "2016-05-03T11:49:08Z"
      @article = Article.new(parameters)
      @article.save
      expect(Article.last.title).to eq "European commission set to approve Turkish citizens' visa-free travel "
      expect(Article.last.language).to eq "english"
      expect(Article.last.body.split.first).to eq "The"
      expect(Article.last.body.split.second).to eq "European"
      expect(Article.last.body.split.last).to eq "parliament?"
    end
  end

  
end

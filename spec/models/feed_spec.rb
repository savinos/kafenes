require 'rails_helper'

RSpec.describe Feed, type: :model do

  context "Δεδομένου URL μεγαλύτερο από 255 χαρακτήρες" do
    it "το @feed δεν είναι έγκυρο" do
      parameters = {}
      url="http://#{"a"*255}.com"
      parameters[:url] = url
      @feed = Feed.new(parameters)
      expect(@feed.valid?).to eq false
    end
  end
  
  context "Δεδομένου URL που περιέχει δεξιά ή αριστερά κενούς χαρακτήρες" do
    it "πριν ελεχθεί το @feed αφαιρούνται" do
      parameters = {}
      url="  http://www.theguardian.com/uk/rss  "
      parameters[:url] = url
      @feed = Feed.new(parameters)
      @feed.save
      expect(Feed.last.url).to start_with "http://www.theguardian.com/uk/rss"
      expect(Feed.last.url).to end_with "http://www.theguardian.com/uk/rss"      
    end
  end

  context "Δεδομένου URL το οποίο δεν ανταποκρίνεται" do
    it "το @feed δεν είναι έγκυρο" do
      url="http://www.priva.cy/"
      @feed = Feed.new(:url => url)
      expect(@feed.valid?).to eq false            
    end
  end
      
  context "Δεδομένου έγκυρου URL" do
    it "με το @feed αποθηκεύεται αυτόματα ο τίτλος" do
      @feed = Feed.new(:url => "http://www.theguardian.com/uk/rss")
      @feed.save
      expect(Feed.last.name).to eq "The Guardian"
    end
  end

  context "Δεδομένου έγκυρου URL" do
    it "με το @feed αποθηκεύεται αυτόματα τελευταία ημερομηνία ανανέωσης" do
      @feed = Feed.new(:url => "http://www.theguardian.com/uk/rss")
      @feed.save
      expect(Feed.last.fetched_at).to eq Date.new            
    end
  end

  context "Δεδομένου κενού URL" do
    it "το @feed δεν είναι έγκυρο" do
      @feed = Feed.new
      expect(@feed.valid?).to eq false            
    end
  end

  context "Δεδομένου URL για κανάλι που υπάρχει ήδη" do
    it "το @feed δεν αποθηκεύεται" do
      @feed = Feed.new(:url => "http://www.theguardian.com/uk/rss" )
      @feed.save
      @feed = Feed.new(:url => "http://www.theguArdiAn.cOm/uk/rss" )
      @feed.save
      expect(Feed.count).to eq 1
    end
  end

  # TODO τα spec του article_should_be_readable να μετακινηθούν στο article_spec
=begin
  context "Δεδομένου URL καναλιού που τα άρθρα δεν είναι αναγνώσιμα" do
    it "το @feed δεν είναι έγκυρο" do
      @feed = Feed.new(:url => "http://www.wired.com/feed/")
      expect(@feed.valid?).to eq false            
    end
  end

  context "Δεδομένου URL καναλιού που τα άρθρα είναι αναγνώσιμα" do
    it "το @feed είναι έγκυρο" do
      @feed = Feed.new( :url => "http://www.theguardian.com/uk/rss")
      expect(@feed.valid?).to eq true            
    end
  end
=end
end

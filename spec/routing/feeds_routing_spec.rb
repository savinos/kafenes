require "rails_helper"

######################################################
# redirect route not found to feeds#index
#

RSpec.describe FeedsController, type: :routing do
  describe "routing" do
    
    it "get feeds route to feeds#index" do
      expect(:get => "/feeds").to route_to("feeds#index")
    end
    
    it "get /feeds/new route to feeds#new" do
      expect(:get => "/feeds/new").to route_to("feeds#new")
    end
    
    
    it "get /feeds/1 not routable" do
      expect(:get => "/feeds/1").not_to be_routable
    end
    
    it "get /feeds/1/edit not routable" do
      expect(:get => "/feeds/1/edit").not_to be_routable
    end

    it "post /feeds route to feeds#create" do
      expect(:post => "/feeds").to route_to("feeds#create")
    end

    it "put /feeds/1 not routable" do
      expect(:put => "/feeds/1").not_to be_routable
    end

    it "patch /feeds/1 not routable" do
      expect(:patch => "/feeds/1").not_to be_routable
    end

    it "delete /feeds/1 route to feeds#destroy" do
      expect(:delete => "/feeds/1").to route_to("feeds#destroy", :id => "1")
    end

  end
end

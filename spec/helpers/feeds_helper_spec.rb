require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the FeedsHelper. For example:
#
# describe FeedsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe FeedsHelper, type: :helper do
  
  context "Δεδομένου URL με ορίσματα στο τέλος" do
    it "η remove_query_from_url επιστρέφει url χωρίς ορίσματα" do
      url="http://feeds.feedburner.com/techcrunch/startups?format=xml"
      new_url = remove_query_from_url(url)
      expect(new_url).to eq("http://feeds.feedburner.com/techcrunch/startups")
    end
  end
  
  context "Δεδομένου url του οποίο το περιεχόμενο δεν είναι σε xml μορφή" do
    it "η is_xml? επιστρέφει false" do
      expect(is_xml? "http://www.wired.com").to eq false          
    end
  end

  context "Δεδομένου url του οποίο το περιεχόμενο είναι σε xml μορφή" do
    it "η is_xml? επιστρέφει false" do
      expect(is_xml? "http://www.wired.com/feed").to eq true          
    end
  end


  context "Δεδομένου url που δεν ανταποκρίνεται σε http αίτημα" do
    it "η is_http_ok? επιστρέφει false" do
      expect(is_http_ok? "http://ucy.ac.cy/el/myuniversityisperfect").to eq false          
    end
  end

  context "Δεδομένου url που ανταποκρίνεται σε http αίτημα" do
    it "η is_http_ok? επιστρέφει true" do
      expect(is_http_ok? "http://www.wired.com/feed").to eq true          
    end
  end


end

FactoryGirl.define do
  factory :article do
    url "MyString"
    title "MyString"
    published_at "2016-05-03 13:30:47"
    language "MyString"
    body "MyText"
  end
end

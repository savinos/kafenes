Δεδομένου(/^ότι υπάρχουν (\d+) άρθρα$/) do |number_of_articles|
  expect(Article.all.length).to eq number_of_articles.to_i
  # TODO use factory girl to generate them
end

Τότε(/^εμφανίζονται (\d+) άρθρα$/) do |number_of_articles|
  find(:row, number_of_articles.to_i)
end

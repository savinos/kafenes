Δεδομένου(/^συμπληρώνω το πεδίο "([^"]*)" με διεύθυνση Καναλιού "([^"]*)"$/) do |field, url|
  case field
  when "URL καναλιού"
    fill_in 'feed_url', :with => url
  else
    pending # express different field
  end
end

Όταν(/^πατώ το ([^"]*) "([^"]*)"$/) do |type, value|
  case type
  when "κουμπί"
    click_button(value)
  when "σύνδεσμο"
    click_link(value)
  else
    pending
  end
end

Όταν(/^το Κανάλι "([^"]*)" έχει καταχωρηθεί$/) do |feed_title|
  expect(Feed.last.name).to eq feed_title
end


Τότε(/^ανακατευθύνομαι στη σελίδα "([^"]*)"$/) do |new_page|
  case new_page
  when "Κανάλια"
    current_path.should == feeds_path
    page.assert_selector('h1', :text => "Κανάλια")
  else
    pending
  end
end

Τότε(/^εμφανίζεται μήνυμα "([^"]*)"$/) do |message|
  expect(page).to have_content(message)
end

# TODO τα υφιστάμενα να γίνονται με χρήση factory - ή ίσως με κάποια εντολή before
Δεδομένου(/^είμαι εγγεγραμένος στο κανάλι "([^"]*)"$/) do |feed|
  case feed
  when "WIRED"
    @feed = Feed.new(:url => "http://www.wired.com/feed/")
    @feed.save
  else
    pending
  end
  # TODO it's hacking - have to remove it
  Model.create
end

Τότε(/^το κανάλι "([^"]*)" να διαγράφεται$/) do |feed_title|
  expect(Feed.where("name='#{feed_title}'").length).to eq 0
end

Δεδομένου(/^(?:ότι |)(?:βρίσκομαι|πηγαίνω) στη σελίδα "([^"]*)"$/) do |arg1|
  Model.create
  case arg1
  when "Εγγραφή σε Νέο Κανάλι"
    visit new_feed_path
  when "Κανάλια"
    visit feeds_path
  when "Αρχική"
    visit root_path
  when "Άρθρα"
    visit articles_path
  else
    pending # express visit for current path
  end
end

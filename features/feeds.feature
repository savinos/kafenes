#language: el
Λειτουργία: Συνδρομές
  
  Θα ήθελα να προσθέτω κάποιες πηγές.
  
  
  Σενάριο: Εγγραφή σε Κανάλι με έγκυρη διεύθυνση
    Δεδομένου ότι βρίσκομαι στη σελίδα "Εγγραφή σε Νέο Κανάλι"
    Και συμπληρώνω το πεδίο "URL καναλιού" με διεύθυνση Καναλιού "http://www.wired.com/feed/"
    Όταν πατώ το κουμπί "Εγγραφή σε Κανάλι"
    Και το Κανάλι "WIRED" έχει καταχωρηθεί
    Τότε ανακατευθύνομαι στη σελίδα "Κανάλια"
    Και εμφανίζεται μήνυμα "Έχετε Εγγραφεί στο Κανάλι με επιτυχία"
    
  Σενάριο: Διαγραφή από Κανάλι
    Δεδομένου είμαι εγγεγραμένος στο κανάλι "WIRED"
    Και πηγαίνω στη σελίδα "Κανάλια"
    Όταν πατώ το σύνδεσμο "Διαγραφή"
    Τότε το κανάλι "WIRED" να διαγράφεται
    Και εμφανίζεται μήνυμα "To Κανάλι έχει διαγραφεί με επιτυχία"

json.array!(@feeds) do |feed|
  json.extract! feed, :id, :name, :url, :fetched_at
  json.url feed_url(feed, format: :json)
end

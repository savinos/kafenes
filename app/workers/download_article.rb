require './config/boot.rb'
require './config/environment.rb'

class DownloadArticle
  include Sidekiq::Worker
  
  sidekiq_options :retry => 0
  def perform(url)
    @article = Article.new(:url => url)
    if @article.valid?
      @article.save
    end
  end
  
end


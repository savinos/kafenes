module FeedsHelper

  #####################################
  # αφήνει μόνο το http ή https, host 
  #   και το path
  def remove_query_from_url(url)
    uri = URI.parse(url)
    return "#{uri.scheme}://#{uri.host}#{uri.path}"
  end


  ##############################################
  # επιστρέφει true όταν το url απαντά
  #   200-OK σε http αίτημα
  def is_http_ok? url
    uri = URI.parse(url)
    res = Net::HTTP.get_response(uri)
    res.code == '200'
  end

  ######################################
  # επιστρέφει true εαν η πρώτη γραμμή
  #   περιέχει τη λέξη xml
  # TODO: έλεγχος δομής xml αν είναι RSS/ATOM
  def is_xml? url
    uri = URI.parse(url)
    res = Net::HTTP.get_response(uri)   
    res.body.lines.first.to_s.include? "xml"
  end
end

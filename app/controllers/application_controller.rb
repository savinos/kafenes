class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_locale, :init_clusters
  
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
  
  def init_clusters
    @clusters = Cluster.where(model_id: Model.last.id)  # .where('number_of_documents >= 5')
  end
end

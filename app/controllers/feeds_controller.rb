class FeedsController < ApplicationController
  include FeedsHelper
  before_action :set_feed, only: [:show, :edit, :update, :destroy]

  # GET /feeds
  # GET /feeds.json
  def index
    @feeds = Feed.paginate(page: params[:page], :per_page => 10)
  end

  # GET /feeds/1
  # GET /feeds/1.json

  def show
  end
  
  # GET /feeds/new
  def new
    @feed = Feed.new
  end

  # GET /feeds/1/edit
  def edit
  end

  # TODO το submit δεν δουλεύει
  # POST /feeds
  # POST /feeds.json
  def create
    @feed = Feed.new
    respond_to do |format|
      @new_url = feed_params[:url]
      if ((Feed.new(feed_params).valid?) &&
          # (@new_url = remove_query_from_url(@new_url)) &&
          (is_xml? @new_url) &&
          ((@feed = Feed.new(:url => @new_url)).save))
        format.html { redirect_to feeds_url, notice: 'Έχετε Εγγραφεί στο Κανάλι με επιτυχία' }
        format.json { render :show, status: :created, location: feeds_url }
      else
        format.html { render :new }
        format.json { render json: @feed.errors, status: :unprocessable_entity }
      end
    end
  end


  

  # PATCH/PUT /feeds/1
  # PATCH/PUT /feeds/1.json

  def update
=begin
    respond_to do |format|
      if @feed.update(feed_params)
        format.html { redirect_to @feed, notice: 'Feed was successfully updated.' }
        format.json { render :show, status: :ok, location: @feed }
      else
        format.html { render :edit }
        format.json { render json: @feed.errors, status: :unprocessable_entity }
      end
    end
=end
  end
  
  # DELETE /feeds/1
  # DELETE /feeds/1.json
  def destroy
    @feed.destroy
    redirect_to(:back)
    respond_to do |format|
      format.html { redirect_to feeds_url, notice: 'To Κανάλι έχει διαγραφεί με επιτυχία' }
      format.json { head :no_content }
    end
    
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_feed
    @feed = Feed.find(params[:id])
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def feed_params
    params.require(:feed).permit(:url)
  end
end

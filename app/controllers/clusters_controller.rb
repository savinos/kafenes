class ClustersController < ApplicationController
  
  def index
  end
  
  def show
    @cluster = Cluster.find(params[:id])
    @keywords = JSON.parse(@cluster.keywords)
    articles_urls = JSON.parse(@cluster.articles_urls)
    @articles = Article.where(:url => articles_urls)
  end
end

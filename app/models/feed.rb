class Feed < ActiveRecord::Base
  before_validation :strip_url
  before_validation :lowercase
  
  validates :url, length: {maximum: 255}
  validates :url, presence: :true, uniqueness: true
  validate :url_should_response
  validate :string_has_uri_regexp
  
  before_save :default_values

  def get_links
    f = Feedjira::Feed.fetch_and_parse(self.url)
    urls = []
    f.entries.each { |e|
      e.url.lstrip!
      e.url.rstrip!
      e.url.downcase!
      uri = Addressable::URI.parse(e.url)
      url = "#{uri.scheme}://#{uri.host}#{uri.path}"
      urls << url
    }
    return urls
  end

  def get_new_articles
    f = Feedjira::Feed.fetch_and_parse(self.url)
    urls = []
    f.entries.each { |e|
      e.url.lstrip!
      e.url.rstrip!
      e.url.downcase!
      uri = Addressable::URI.parse(e.url)
      url = "#{uri.scheme}://#{uri.host}#{uri.path}"
      urls << url
    }
    old = Article.pluck(:url).flatten.uniq
    new_urls = urls.sort.uniq - old 
    return new_urls
  end
  
  private
  def strip_url
    if(self.url)
      self.url.lstrip!
      self.url.rstrip!
    end
  end

  def lowercase
    if self.url
      self.url.downcase!
    end
  end

  ########################################################
  # Ελέγχεται με το μέγεθος της απάντησης του ping
  # θα μπορούσε εδώ να εκμεταλλεύεται περισσότερες πληροφορίες
  # από την απάντηση.
  def url_should_response
    if ( (self.url) &&
         (self.url  =~ /\A#{URI::regexp(['http', 'https'])}\z/) &&
         (`ping -c 1 #{URI.parse(self.url).host}`.length == 0))
      errors.add(:url, "δεν ανταποκρίνεται, υπάρχει;")
    end
  end
  
  #########################################################
  # regex from stackoverflow and it seems ok
  # http://stackoverflow.com/questions/1805761/check-if-url-is-valid-ruby
  def string_has_uri_regexp
    if !(url =~ /\A#{URI::regexp(['http', 'https'])}\z/)
      errors.add(:url, "δεν έχει τη κανονική μορφή uri")
    end
  end

  def default_values
    self.name ||= Feedjira::Feed.fetch_and_parse(self.url).title
    self.fetched_at ||= Date.new
  end
end

# TODO να προσθέσω των αριθμό των φορών που συναντιέται κάθε λέξη στη βάση
# TODO να αρχικοποιείται με το κατέβασμα του άρθρου

class Article < ActiveRecord::Base
  before_validation :remove_queries_from_url
  
  validates :url, presence: :true, uniqueness: true
  validate :article_should_be_readable
  validate :article_should_not_be_small
  # το πιο πάνω θα μπορεί να ελέγχει
  #   το xml αν έχει κάποιο περιεχόμενο εναλλαχτικά
  before_save :default_values
  
  private

  ######################################
  # Επιστρέφει true αν το άρθο
  #  είναι αναγνώσιμα από την εφαρμογή
  def article_should_be_readable
    if (Pismo::Document.new(self.url).body.blank? == true)
      errors.add(:url, "δεν είναι αναγνώσιμο")
    end

  end

  def article_should_not_be_small
    if (Pismo::Document.new(self.url).body.split.length < 200)
      errors.add(:url, "είναι μικρό κείμενο")
    end

  end


  ########################################
  # Επιστρέφει το url χωρίς ορίσματα
  #   μετά το path. Είναι διαφορετικό
  #   από αυτό του feeds_helper.
  def remove_queries_from_url
    self.url.lstrip!
    self.url.rstrip!
    uri = URI.parse(self.url)
    self.url = "#{uri.scheme}://#{uri.host}#{uri.path}"
  end
  
  
  def default_values
    doc = Pismo::Document.new(self.url)
    self.title ||= doc.title.split("|").first # split μόνο για the guardian
    self.published_at ||= doc.datetime
    self.body = doc.body.gsub(/[^0-9A-Za-z .;,!?]/, '')
    w = WhatLanguage.new(:all)
    self.language = w.language(self.body).to_s
    
  end

end

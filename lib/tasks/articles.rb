BOW_REDUCTION_PERCENTAGE = 0.3
require 'narray'
require 'tf-idf-similarity'

namespace :articles do
  desc "Κατέβασε όλα τα καινούργια άρθρα από τα κανάλια"
  task fetch: :environment do
    # OPTIMIZE minimize http request response
    urls = []
    Feed.all.each { |f|
      urls.concat(f.get_new_articles)
    }
    
    urls.uniq!
    
    urls.shuffle.each { |u|
      DownloadArticle.perform_async(u)
    }    
  end

  desc "Κατέβασε όλα τα καινούργια άρθρα από τα κανάλια"
  task count_last_news: :environment do
    from_days_before = 3
    to_days_before = 0
    count = Article.where(published_at: (Time.now.midnight - from_days_before.day)..(Time.now.midnight - to_days_before.day)).count
    puts "Last #{from_days_before} days: #{count} new articles"
  end
  desc "Ομαδοποίηση Άρθρων"
  task divide_and_conquer: :environment do
    from_days_before = 3
    to_days_before = 0
    language = "english"
    # Πέρνει όλα τα άρθρα
    @corpus = []
    Article.where(:language => language).where(published_at: (Time.now.midnight - from_days_before.day)..(Time.now.midnight - to_days_before.day)).each_with_index { |article, index|
      document = MyFilter.filter(article.body)
      @corpus << TfIdfSimilarity::Document.new(document, :urls => [article.url], :ids => [index]) 
    }

    # Δημιουργεί το μοντέλλο και similarity matrix
    model = TfIdfSimilarity::TfIdfModel.new(@corpus, :library => :narray)
    matrix = model.similarity_matrix
    
    @working_matrix = MyNarray.new_narray(matrix)

    # υπολογισμός μέσου και stddev 
    mean = MyNarray.mean(matrix)
    stddev = MyNarray.stddev(matrix)
    threshold = mean + stddev
    reduced_threshold = mean + (stddev/2.0)
    # δημιουργία clusters
    loop do
      candidate_to_merge = MyNarray.sorted_indexes_larger_than(@working_matrix,threshold)
      break if candidate_to_merge.length == 0
      merged_flag = 0
      puts @working_matrix.shape[0]
      candidate_to_merge.each { |m|
        furthest = MyNarray.furthest_similarity(matrix, @corpus[m[0]].ids, @corpus[m[1]].ids)
        if furthest >= reduced_threshold
          model.merge_and_update(m[0], m[1])
          merged_flag = 1
          break
        end
      }
      break if merged_flag == 0
      @working_matrix = model.similarity_matrix
    end

    # αποθήκευση μοντέλλου και clusters στη βάση
    @save_model = Model.create(:model_json => model.to_json)
    model.documents.each_with_index { |d,i|
      Cluster.create(:model_id => @save_model.id, :articles_urls => d.urls.to_json, :keywords => model.keywords(i), :number_of_documents => d.urls.length)
      puts "Cluster #{i} #{model.keywords(i)}"
      puts "============"
      puts d.urls
    }
    
  end
  
end

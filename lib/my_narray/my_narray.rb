require 'narray'

module MyNarray
  # include NArray

  def MyNarray.transform_to_echelon(matrix)
    return if matrix.shape[0] != matrix.shape[1]
    transformed_matrix = MyNarray.new_narray matrix
    indexes_to_be_zero = []
    k = transformed_matrix.shape[0]
    0.upto(k-1) { |i|
      0.upto(i%k) { |j|
        indexes_to_be_zero << i*k+j
      }
    }
    transformed_matrix[ indexes_to_be_zero ] = 0
    return transformed_matrix
  end

  def MyNarray.new_narray matrix
    new_matrix = NArray.to_na(matrix.to_a)
    return new_matrix
  end

  def MyNarray.sorted_indexes_larger_than(matrix, limit)
    return if matrix.shape[0] != matrix.shape[1]
    temp_matrix = MyNarray.transform_to_echelon matrix
    number_of_indexes = temp_matrix.sort.flatten.to_a.count { |x| x >= limit}
    raw_indexes = temp_matrix.sort_index.flatten.to_a.last(number_of_indexes)
    final_indexes = []
    raw_indexes.reverse.each { |i|
      a = i / matrix.shape[0]
      b = i % matrix.shape[0]
      final_indexes << [a,b]
    }
    return final_indexes
  end

  def MyNarray.furthest_similarity(matrix, c1, c2 )
    return if matrix.shape[0] != matrix.shape[1]
    temp_matrix = MyNarray.new_narray matrix
    
    compination = c1.product(c2) + c2.product(c1)
    
    sim = []
    compination.each { |c|
      sim << temp_matrix[c[0],c[1]]
    }
    
    sim = sim.sort.delete_if { |x| x < 0.000001}
    return sim[0]
  end

  def MyNarray.mean(matrix)
    temp_matrix = MyNarray.new_narray matrix
    temp_matrix = MyNarray.transform_to_echelon temp_matrix
    mean = NArray.to_na(temp_matrix.flatten.to_a.delete_if {|x| x< 0.001 }).mean
    return mean
  end

  def MyNarray.stddev(matrix)
    temp_matrix = MyNarray.new_narray matrix
    temp_matrix = MyNarray.transform_to_echelon temp_matrix
    stddev = NArray.to_na(temp_matrix.flatten.to_a.delete_if {|x| x< 0.001 }).stddev
    return stddev
  end
  
end

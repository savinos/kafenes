module MyFilter

  def MyFilter.filter(document)
    sieve = Stopwords::Snowball::WordSieve.new
    return document.downcase.split(/\W+/).delete_if{|w| /^\p{N}/.match(w)}.map{|w| Stemmer::stem_word(w)}.delete_if{ |w| sieve.stopword? lang: :en, word: w}.join(" ")
  end


end

class AddModelRefToClusters < ActiveRecord::Migration
  def change
    add_reference :clusters, :model, index: true, foreign_key: true
  end
end

class AddColumnsNumberOfDocumentsDocumentFrequencyStringToModel < ActiveRecord::Migration
  def change
    add_column :models, :number_of_documents, :integer
    add_column :models, :document_frequency_string, :string
  end
end

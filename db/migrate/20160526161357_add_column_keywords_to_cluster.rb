class AddColumnKeywordsToCluster < ActiveRecord::Migration
  def change
    add_column :clusters, :keywords, :string
  end
end

class ChangeFeedColumns < ActiveRecord::Migration
  def change
    rename_column :feeds, :last_fetched, :fetched_at
    rename_column :feeds, :title, :name
    change_column :feeds, :url, :string
  end
end

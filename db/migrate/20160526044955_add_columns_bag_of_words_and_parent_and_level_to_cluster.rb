class AddColumnsBagOfWordsAndParentAndLevelToCluster < ActiveRecord::Migration
  def change
    add_column :clusters, :bag_of_words_text, :text
    add_reference :clusters, :cluster
    add_column :clusters, :level, :integer
  end
end

class RemoveColumnModelAgainFromCluster < ActiveRecord::Migration
  def change
    remove_column :clusters, :model, :text
  end
end

class AddArticlesToCluster < ActiveRecord::Migration
  def change
    add_column :clusters, :articles_ids_csv, :string
  end
end

class RemoveColumnArticlesTextFromCluster < ActiveRecord::Migration
  def change
    remove_column :clusters, :articles_text
  end
end

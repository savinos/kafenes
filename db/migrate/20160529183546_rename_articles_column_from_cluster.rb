class RenameArticlesColumnFromCluster < ActiveRecord::Migration
  def change
    rename_column :clusters, :articles_ids_csv, :articles_urls
  end
end

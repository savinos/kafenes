class RenameTermsToBagOfWordsFromModel < ActiveRecord::Migration
  def change
    rename_column :models, :terms, :bag_of_words_text
  end
end

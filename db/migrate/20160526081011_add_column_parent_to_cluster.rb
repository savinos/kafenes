class AddColumnParentToCluster < ActiveRecord::Migration
  def change
    add_column :clusters, :parent_cluster_id, :integer
  end
end

class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :url
      t.string :title
      t.datetime :published_at
      t.string :language
      t.text :body

      t.timestamps null: false
    end
    add_index :articles, :url, unique: true
  end
end

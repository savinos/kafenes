class RemoveColumnsFromCluster < ActiveRecord::Migration
  def change
    remove_column :clusters, :bag_of_words_text, :text
    remove_column :clusters, :bag_of_words_reduced_text, :text
    remove_column :clusters, :document_frequency_reduced_string, :string
    remove_column :clusters, :document_frequency_string, :string
    remove_column :clusters, :weight_sum_string, :string
  end
end

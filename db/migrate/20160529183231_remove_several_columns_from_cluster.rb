class RemoveSeveralColumnsFromCluster < ActiveRecord::Migration
  def change
    remove_column :clusters, :bow_df_json, :text
    remove_column :clusters, :bow_df_reduced_json, :text
    remove_column :clusters, :bow_w_json, :text
    remove_column :clusters, :bow_w_reduced_json, :text
    remove_column :clusters, :level, :text
  end
end

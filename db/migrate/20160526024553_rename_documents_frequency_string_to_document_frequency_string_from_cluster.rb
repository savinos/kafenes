class RenameDocumentsFrequencyStringToDocumentFrequencyStringFromCluster < ActiveRecord::Migration
  def change
    rename_column :clusters, :documents_frequency_string, :document_frequency_string
  end
end

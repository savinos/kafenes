class RemoveModelFromClusters < ActiveRecord::Migration
  def change
    remove_reference :clusters, :model, index: true, foreign_key: true
  end
end

class AddColumnsToCluster < ActiveRecord::Migration
  def change
    add_column :clusters, :bow_df_json, :text
    add_column :clusters, :bow_w_json, :text
    add_column :clusters, :bow_df_reduced_json, :text
    add_column :clusters, :bow_w_reduced_json, :text
  end
end

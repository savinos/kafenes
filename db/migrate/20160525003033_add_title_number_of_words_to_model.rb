class AddTitleNumberOfWordsToModel < ActiveRecord::Migration
  def change
    add_column :models, :title, :string
    add_column :models, :number_of_words, :integer
  end
end

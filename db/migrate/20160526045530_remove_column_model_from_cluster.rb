class RemoveColumnModelFromCluster < ActiveRecord::Migration
  def change
    remove_reference :clusters, :model
  end
end

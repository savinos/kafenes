class CreateClusters < ActiveRecord::Migration
  def change
    create_table :clusters do |t|
      t.integer :number_of_documents, :default => 0
      t.string :documents_frequency_string
      t.string :weight_sum_string
      t.text :articles_text
                                
      t.timestamps null: false
    end
  end
end

class RemoveParentAndIdsColumnsFromCluster < ActiveRecord::Migration
  def change
    remove_column :clusters, :cluster_id, :integer
    remove_column :clusters, :parent_cluster_id, :integer
  end
end

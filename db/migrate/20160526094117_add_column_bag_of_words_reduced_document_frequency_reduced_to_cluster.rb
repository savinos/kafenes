class AddColumnBagOfWordsReducedDocumentFrequencyReducedToCluster < ActiveRecord::Migration
  def change
    add_column :clusters, :bag_of_words_reduced_text, :text
    add_column :clusters, :document_frequency_reduced_string, :string
  end
end

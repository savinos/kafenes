class AddTermsArrayToModel < ActiveRecord::Migration
  def change
    add_column :models, :terms, :text
  end
end

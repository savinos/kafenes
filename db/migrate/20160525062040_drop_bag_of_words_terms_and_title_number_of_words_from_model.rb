class DropBagOfWordsTermsAndTitleNumberOfWordsFromModel < ActiveRecord::Migration
  def change
    drop_table :bag_of_words
    drop_table :terms
    remove_column :models, :number_of_words
    remove_column :models, :title    
  end
end

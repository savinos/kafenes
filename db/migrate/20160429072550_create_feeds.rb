class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
      t.text :title
      t.text :url
      t.datetime :last_fetched
      t.timestamps null: false
    end
    add_index :feeds, :url, unique: true
  end
end
# DONE change feeds tbl- title to name
# DONE change feeds tbl- title text to string
# DONE change feeds tbl- last_fetched to fetched_at
